db.fruits.aggregate([
    { $match : { onSale: true } },
    { $count: "Total # of fruits for sale" }
])