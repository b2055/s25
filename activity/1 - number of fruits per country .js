db.fruits.aggregate([
    { $unwind: "$origin"  },
    { $group: { _id: "$origin", count: { $sum:1 } } }
])