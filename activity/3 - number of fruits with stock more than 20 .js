db.fruits.aggregate([
    { $match : { stock : { $gte: 20 } } },
    { $count: "Total # of fruits with stock more than 20" }
])